#-------------------------------------------------
#
# Project created by QtCreator 2018-04-28T10:56:45
#
#-------------------------------------------------

QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17
TARGET = DOS2XML
TEMPLATE = app
win32{
QMAKE_TARGET_PRODUCT += Divinity Original Sin - XML Editor
QMAKE_TARGET_COPYRIGHT += Kassany
VERSION += 1.0.1.2
VERSION_PE_HEADER = 1.02
QMAKE_TARGET_DESCRIPTION += XML Translator
}
QMAKE_CXXFLAGS += -mindirect-branch=thunk-inline -mindirect-branch-register -mindirect-branch=thunk
# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

RC_ICONS += $$PWD/icon.ico

SOURCES += \
        main.cpp \
        divinity_translator.cpp

HEADERS += \
        divinity_translator.hpp

FORMS += \
        divinity_translator.ui
