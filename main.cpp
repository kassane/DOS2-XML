#include <QApplication>
#include <QTextCodec>
#include "divinity_translator.hpp"

int main(int argc, char *argv[]) {
  // QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
  QApplication a(argc, argv);
  a.setAttribute(Qt::AA_EnableHighDpiScaling);
  Divinity_Translator w;
  w.setWindowTitle(QObject::tr("Divinity Original Sin 2 - Translator"));
  a.setStyle("fusion");
  w.show();

  return a.exec();
}
