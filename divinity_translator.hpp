#ifndef DIVINITY_TRANSLATOR_HPP
#define DIVINITY_TRANSLATOR_HPP

#pragma once
#include <QWidget>

namespace Ui {
class Divinity_Translator;
}
class QStandardItemModel;

class Divinity_Translator : public QWidget {
  Q_OBJECT

public:
  explicit Divinity_Translator(QWidget *parent = nullptr);
  void Savefile();
  void Openfile();
  ~Divinity_Translator();

private slots:
  void on_openBtn_clicked();
  void on_saveBtn_clicked();
  void on_findBtn_clicked();
  void on_replBtn_clicked();

private:
  Ui::Divinity_Translator *ui;
  QString tagName = "", filename = "";
  QStringList _attrN, _attrV;
  QStandardItemModel *model;
  class QSortFilterProxyModel *pmodel;
};

#endif // DIVINITY_TRANSLATOR_HPP
