#include "divinity_translator.hpp"
#include "ui_divinity_translator.h"
#include <QAbstractItemModel>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QTextStream>
#include <QtXml>

Divinity_Translator::Divinity_Translator(QWidget *parent)
    : QWidget(parent), ui(new Ui::Divinity_Translator) {
  ui->setupUi(this);
  setMinimumSize(QSize(800, 600));

  ui->fileEdit->setClearButtonEnabled(true);
  ui->findEdit->setClearButtonEnabled(true);

  // DEVEL MSG
  //  ui->replBtn->setDisabled(true);
  //  ui->replEdit->setText("Coming soom! Development");
  //  ui->replEdit->setDisabled(true);

  connect(ui->closeBtn, &QPushButton::clicked, this, [] { qApp->quit(); });

  model = new QStandardItemModel(this);
  model->setHorizontalHeaderLabels(QStringList() << tr("Strings"));
  ui->tableView->horizontalHeader()->setStretchLastSection(true); // autoresize
  // ui->tableView->verticalHeader()->setHidden(true);
  ui->tableView->setColumnWidth(0, 30);

  connect(ui->findEdit, &QLineEdit::textChanged, [this]() {
    // Se clicar no botão de limpeza ele altera o valor e o resultado.
    if (!ui->findEdit->text().isEmpty())
      return;
    ui->tableView->setModel(model);
  });
  ui->tableView->setModel(model);
}

void Divinity_Translator::Savefile() {
  filename = QFileDialog::getSaveFileName(this, tr("Save XML files"),
                                          QDir::currentPath(), "XML(*.xml)");
}

void Divinity_Translator::Openfile() {
  ui->fileEdit->setText(QFileDialog::getOpenFileName(
      this, tr("Open XML files"), QDir::currentPath(), "XML(*.xml)"));
}

Divinity_Translator::~Divinity_Translator() { delete ui; }

void Divinity_Translator::on_openBtn_clicked() {
  Openfile();
  QDomDocument xdoc(QFileInfo(ui->fileEdit->text()).baseName());
  QFile _xml(ui->fileEdit->text());
  if (!_xml.open(QIODevice::ReadOnly))
    return;
  if (!xdoc.setContent(&_xml)) {
    _xml.close();
    return;
  }
  _xml.close();

  QDomElement docElem = xdoc.documentElement();

  QDomNode n = docElem.firstChild();
  int row = 0;
  QStringList ss;
  while (!n.isNull()) {
    QDomElement e = n.toElement(); // try to convert the node to an element.
    if (!e.isNull()) {
      tagName = e.tagName();
      ss << e.text(); // the node really is an element.
    }

    model->setItem(row, 0, new QStandardItem(ss.value(row)));
    n = n.nextSibling();
    ++row;
  }
  QDomNodeList start = xdoc.elementsByTagName(tagName);
  for (int i = 0; i < row; ++i) {
    QDomNode node = start.item(i);
    auto map = node.attributes();
    for (int j = 0; j < map.length(); j++) {
      auto mapItem = map.item(j);
      auto attribute = mapItem.toAttr();
      _attrN << attribute.name();
      _attrV << attribute.value();
    }
  }
  model->setHorizontalHeaderLabels(QStringList() << tr("Strings"));
  ui->tableView->horizontalHeader()->setStretchLastSection(true); // autoresize
  // ui->tableView->verticalHeader()->setHidden(true);
  ui->tableView->setColumnWidth(0, 30);

  // ui->tableView->update();
  QMessageBox::information(
      this, tr("Successful"),
      QString(tr("%1 was Loaded!"))
          .arg(QFileInfo(ui->fileEdit->text()).baseName()));
}

void Divinity_Translator::on_saveBtn_clicked() {
  Savefile();
  QDomDocument doc(QFileInfo(filename).baseName());
  int row = model->rowCount();
  int col = model->columnCount();
  QFile sXml(filename);
  if (!sXml.open(QIODevice::WriteOnly | QIODevice::Text))
    return;

  QDomElement root = doc.createElement("contentList");
  QTextStream stream(&sXml);
  for (int i = 0; i < row; ++i) {
    QDomElement ex = doc.createElement(tagName);
    for (int j = 0; j < col; j++) {
      QDomText de = doc.createTextNode(model->index(i, j).data().toString());
      ex.appendChild(de);
      ex.setAttribute(_attrN.value(i), _attrV.value(i));
    }
    root.appendChild(ex);
  }
  doc.appendChild(root);
  stream << doc.toString();
  sXml.close();
  QMessageBox::information(
      this, tr("Successful"),
      QString(tr("%1 was Saved!")).arg(QFileInfo(filename).baseName()));
}

void Divinity_Translator::on_findBtn_clicked() {
  pmodel = new QSortFilterProxyModel(this);
  pmodel->setSourceModel(model);
  pmodel->setFilterKeyColumn(0);
  pmodel->setFilterFixedString(ui->findEdit->text());
  ui->tableView->setModel(pmodel);
}

void Divinity_Translator::on_replBtn_clicked() {
  for (int row = ui->tableView->currentIndex().row(); row < pmodel->rowCount();
       ++row)
    for (int col = ui->tableView->currentIndex().column();
         col < pmodel->columnCount(); col++) {
      model->setItem(
          row, col,
          new QStandardItem(pmodel->index(row, col).data().toString().replace(
              pmodel->index(row, col).data().toString(),
              ui->replEdit->text())));
      qDebug() << "Contents:\n"
               << pmodel->index(row, col).data().toString() << " > "
               << model->index(row, col).data().toString();
    }
}
